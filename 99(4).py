for i in range(1, 10):
    for k in range(1, 10-i):
        print(end='\t'*3)
    for m in range(10-i, 10):
        print(str(i) + ' * ' + str(m) + ' = ' + str(m * i), end='\t')
    print()
print('*' * 100)
for i in range(1, 10):
    for k in range(1, i):
        print(end='\t'*3)
    for m in range(i, 10):
        print(str(i) + ' * ' + str(m) + ' = ' + str(m * i), end='\t')
    print()