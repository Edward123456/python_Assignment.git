-- MySQL dump 10.13  Distrib 5.7.22, for Win32 (AMD64)
--
-- Host: localhost    Database: blogdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.35-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id号',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `abstract` varchar(200) NOT NULL COMMENT '摘要',
  `content` text NOT NULL COMMENT '博文内容',
  `uid` int(10) unsigned DEFAULT NULL COMMENT '用户标识',
  `pcount` int(10) unsigned DEFAULT '0' COMMENT '点赞数',
  `flag` tinyint(3) unsigned DEFAULT '0' COMMENT '状态,0''新建'',1''发布'',2''删除''',
  `cdate` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (1,'b1','b1','dfdfffff',1,1,2,'2018-09-25 00:00:00'),(2,'b2','b2','aaaaaaaaaaaaaaaaaaaaaaa',2,32,0,'2018-09-26 00:00:00'),(3,'b3','b3','ssssssssssssssssssss',2,43,0,'2018-09-21 00:00:00'),(4,'b4','b4','22222222222222222',4,43,0,'2018-09-18 00:00:00'),(5,'b5','b5','gggggggggggggg',5,43,0,'2018-02-24 00:00:00'),(6,'b6','b6','ggggggggggggg',6,34,0,'2018-08-12 00:00:00'),(7,'b7','b7','dddddddddddddddddd',3,43,0,'2018-07-08 00:00:00'),(8,'b8','b8','wwwwwwwwwwwww',5,324,0,'2018-07-16 00:00:00'),(9,'b9','b9','333333333333333333',3,342,0,'2018-05-06 00:00:00'),(10,'b10','b10','ffffdwdfw',8,34,0,'2018-09-01 00:00:00');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id 号',
  `name` varchar(32) NOT NULL COMMENT '姓名',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱地址',
  `cdate` datetime DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `NewIndex1` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'zhangsan','12','2018-09-01 00:00:00'),(2,'lisi','23','2018-09-03 00:00:00'),(3,'wangwu','24','2018-09-09 00:00:00'),(4,'zhaoliu','45','2018-09-10 00:00:00'),(5,'q1','56','2018-09-30 00:00:00'),(6,'q2','43','2018-08-09 00:00:00'),(7,'q3','32','2018-08-01 00:00:00'),(8,'q4','12','2018-07-09 00:00:00'),(9,'q5','43','2018-07-01 00:00:00'),(10,'q6','423','2018-07-01 00:00:00'),(11,'q7','32','2018-10-10 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-25 23:28:37
