i = 1
while i < 10:
    m = 1
    while m <= i:
        print(str(m) + ' * ' + str(i) + ' = ' + str(m * i), end='\t')
        m += 1
    i += 1
    print()
print('*' * 100)
i = 1
while i < 10:
    m = i
    while m < 10:
        print(str(i) + ' * ' + str(m) + ' = ' + str(m * i), end='\t')
        m += 1
    i += 1
    print()
