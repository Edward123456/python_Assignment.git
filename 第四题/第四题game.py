# 导入相应模块
import pygame
from pygame.locals import *
import time,random

class HeroPlane:
    '''玩家飞机'''
    def __init__(self,screen_temp):
        self.x = 200
        self.y = 400
        self.screen = screen_temp
        self.image = pygame.image.load('./images/me.png')
        self.bullet_list = []   # 用于存放玩家的子弹列表
    def display(self):
        '''绘制玩家飞机子弹'''
        for b in self.bullet_list:
            b.display()
            if b.hero_move():
                self.bullet_list.remove(b)
        '''绘制玩家飞机'''
        self.screen.blit(self.image,(self.x,self.y))
    def move_left(self):
        '''左移飞机'''
        self.x -= 5
        if self.x < 0:
            self.x = 0
    def move_right(self):
        '''右移飞机'''
        self.x += 5
        if self.x > 406:
            self.x = 406
    def fire(self):
        self.bullet_list.append(Bullet(self.screen,self.x,self.y))


class EnemyPlane:
    '''敌机'''
    def __init__(self,screen_temp):
        self.x = random.choice(range(408))
        self.y = -75
        self.screen = screen_temp
        self.image = pygame.image.load('./images/e'+str(random.choice(range(3)))+'.png')
        self.bullet_list = []   # 用于存放玩家的子弹列表
    def display(self):
        '''绘制敌机子弹'''
        for b in self.bullet_list:
            b.display()
            if b.enemy_move():
                self.bullet_list.remove(b)
        '''绘制敌机'''
        self.screen.blit(self.image,(self.x,self.y))
    def move(self,hero):
        self.y += 4
        # 敌机是否出屏幕检测
        if self.y > 642:
            return True
        # 敌机是否中弹检测
        for bo in hero.bullet_list:
            if bo.x>self.x+12 and bo.x<self.x+92 and bo.y>self.y+20 and bo.y<self.y+60:
                hero.bullet_list.remove(bo)
                return True

    def fire(self,hero):
        self.bullet_list.append(Bullet(self.screen, self.x, self.y))
        # 检测英雄机是否和敌机子弹相撞
        for enemy_bo in self.bullet_list:
            if enemy_bo.y > 420 and enemy_bo.y < 450 and enemy_bo.x < hero.x + 100 and enemy_bo.y > hero.x + 10:
                self.bullet_list.remove(enemy_bo)
                return True

class Bullet:
    '''子弹'''
    def __init__(self,screen_temp,x,y):
        self.x = x + 53
        self.y = y
        self.screen = screen_temp
        self.image = pygame.image.load('./images/pd.png')
    def display(self):
        self.screen.blit(self.image,(self.x,self.y))
    def hero_move(self):
        self.y -= 10
        if self.y < -20:
            return True
    def enemy_move(self):
        self.y += 6
        if self.y > 585:
            return True

def key_control(hero_temp):
    '''键盘控制函数'''
    # 执行退出操作
    for event in pygame.event.get():
        if event.type == QUIT:
            exit()
    # 获取按键信息
    pressed_keys = pygame.key.get_pressed()
    # 做判断，并执行对象操作
    if pressed_keys[K_LEFT] or pressed_keys[K_a]:
        hero_temp.move_left()
    if pressed_keys[K_RIGHT] or pressed_keys[K_d]:
        hero_temp.move_right()
    if pressed_keys[K_SPACE]:
        hero_temp.fire()

def main():
    # 创建游戏窗口
    screen = pygame.display.set_mode((512,568),0,0)
    #创建游戏背景
    background = pygame.image.load('./images/bg2.jpg')
    # 创建玩家飞机
    hero = HeroPlane(screen)
    m = -968
    enemy_list = [] #存放敌机列表
    while True:
        # 绘制画面
        screen.blit(background,(0,m))
        m += 2
        if m>= -200:
            m = -968
        # 绘制玩家飞机
        hero.display()
        # 执行键盘控制
        key_control(hero)
        # 随机绘制敌机
        if random.choice(range(50)) == 10:
            enemy_list.append(EnemyPlane(screen))
        # 遍历敌机，并绘制移动
        for em in enemy_list:
            # 敌机发射子弹
            if random.choice(range(30)) == 9:
                if em.fire(hero):
                    pygame.quit()
            em.display()
            # 检测敌机是否和英雄机相撞
            if em.x+12 < hero.x+100 and em.x+92 > hero.x+10 and em.y+60 > 420 and em.y < 450:
                enemy_list.remove(em)
                pygame.quit()
            # 如果敌机飞出屏幕或者敌机中弹，则清除该敌机对象
            if em.move(hero):
                enemy_list.remove(em)

        # 更新显示
        pygame.display.update()
        # 定时显示
        time.sleep(0.04)

# 判断当前是否是主运行程序，并调用
if  __name__ == '__main__':
    main()