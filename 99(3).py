i = 1
while i < 10:
    k = 1
    while k < 10 - i:
        print(end='\t'*3)
        k += 1
    m = 10 - i
    while m < 10:
        print(str(i) + ' * ' + str(m) + ' = ' + str(m * i), end='\t')
        m += 1
    i += 1
    print()
print('*' * 100)
i = 1
while i < 10:
    k = 1
    while k < i:
        print(end='\t'*3)
        k += 1
    m = i
    while m < 10:
        print(str(i) + ' * ' + str(m) + ' = ' + str(m * i), end='\t')
        m += 1
    i += 1
    print()
