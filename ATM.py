# 银行后台数据信息
BankData = [
    {'name': 'zhangsan', 'balance': 100},
    {'name': 'lisi', 'balance': 150},
    {'name': 'wangwu', 'balance': 250}
]
# 登录状态初始值为N
logon = 'N'
while True:
    # 输出初始界面
    print('*' * 12 + '自动取款机的存取款模拟效果' + '*' * 13)
    print('{0:1}{1:10}{2:10}'.format(' ', '1.登陆/退出', '2.查询余额'))
    print('{0:1}{1:12}{2:10}'.format(' ', '3.取钱', '4.存钱'))
    print('*' * 50)
    # 根据键盘值判断并执行对应操作
    key = input('please select your option:')
    # 登陆/退出
    if key == '1':
        # 执行登陆或退出的命令
        if logon == 'N':
            # 检索登陆信息是否有效，若无效则继续输入
            while (logon == 'N'):
                name = input('input your name:')
                for i in range(len(BankData)):
                    if BankData[i]['name'] == name:
                        # 如果登录成功，更改登录状态为N
                        logon = 'Y'
                        print('Dear ' + name + ', welcome to swiss Bank')
                        # 登陆成功跳出遍历循环
                        break
        else:
            # 如果登陆以后再次选1，则为执行退出操作
            logon = 'N'
            print('Dear ' + name + ',look forward to your next time,ByeBye')
        # input('enter will continue')
    # 查询余额
    elif key == '2':
        # 如果当前状态为已登录，则可执行2/3/4操作，否则需要先进行账户登录操作
        if logon == 'Y':
            print('Your Balance is ' + str(BankData[i]['balance']) + '$')
        else:
            print('please be logon first')
        # input('enter will continue')
    # 取款
    elif key == '3':
        print('Withdraw your Money')
        if logon == 'Y':
            # 输入要取得钱数，并更改当前账户余额
            cash_w = input('how much money will you want to withdraw?')
            BankData[i]['balance'] = BankData[i]['balance'] - int(cash_w)
            print('this Transaction you have been withdrawed ' + cash_w + '$ ,and your Banlance is ' + str(
                BankData[i]['balance']) + '$')
        else:
            print('please be logon first')
        # input('enter will continue')
    # 存款
    elif key == '4':
        print('Save Money')
        if logon == 'Y':
            # 输入要存得钱数，并更改当前账户余额
            cash_s = input('how much money will you want to withdraw?')
            BankData[i]['balance'] = BankData[i]['balance'] + int(cash_s)
            print('this Transaction you have been saved ' + cash_s + ' ,and your Banlance is ' + str(
                BankData[i]['balance']) + '$')
        else:
            print('please be logon first')
        # input('enter will continue')
    else:
        print('*' * 2 + ' no valid input,please input 1/2/3/4 option to process,many thanks ' + '*' * 2)
