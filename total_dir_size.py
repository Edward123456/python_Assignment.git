# 统计指定目录大小的函数
import os
# 定义全局变量
length = 0

def dir_size(dir):
    global length
    # 获取被复制目录中的所有文件信息
    dlist = os.listdir(dir)
    # 遍历所有文件，并对所有文件大小加和统计
    for f in dlist:
        file = os.path.join(dir, f)
        # 判断是否为文件
        if os.path.isfile(file):
            length += os.path.getsize(file)
        # 判断是否为目录
        if os.path.isdir(file):
            dir_size(file)  # 递归调用自己，来实现子目录的大小统计

dir = input('input the absolute director: ')
dir_size(dir)
print('this director size is ' + str(length))
