for i in range(1, 10):
    for m in range(1, i + 1):
        print(str(m) + ' * ' + str(i) + ' = ' + str(m * i), end='\t')
    print()
print('*' * 100)
for i in range(1, 10):
    for m in range(i, 10):
        print(str(i) + ' * ' + str(m) + ' = ' + str(m * i), end='\t')
    print()
