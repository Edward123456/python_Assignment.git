# mysql的数据修改
# 导入mysql模块
import pymysql
class stu_opt:
    def __init__(self):
        # 创建数据库连接
        self.db = pymysql.connect(host='localhost', user='root', password='', db='mydemo', charset='utf8')
        # 创建游标对象
        self.cursor = self.db.cursor()
    def mysql(self,sql):
        try:
            # 执行sql操作
            m = self.cursor.execute(sql)
            # 事物提交
            self.db.commit()
            return m

        except Exception as err:
            # 事务回滚
            self.db.rollback()
            print('SQL执行错误！原因:', err)

    # findAll( )--查询方法 、delete（id）-- 删除方法   insert（data）--添加方法
    def findAll(self,data):
        m = self.mysql('select * from stu where classid="%s"'%(data))
        print('查询结果条数：', m)

    def delete(self,data):
        m = self.mysql("delete from stu where id='%d'"%(data))
        print('删除结果条数：', m)

    def insert(self,data):
        m = self.mysql("insert into stu(name,age,sex,classid) values('%s','%d','%s','%s')" % (data))
        print('成功添加条数：', m)

    def __del__(self):
        # 关闭数据库连接
        self.db.close()

# 定义sql语句
def main():
    s = stu_opt()
    while True:
        print('=='*20)
        print('please input your option:')
        print('1.select\n2.delete\n3.insert')
        print('==' * 20)
        opt = input('opt:')
        # 查询
        if int(opt) == 1:
            data = input('input the classid which u want to query:')
            s.findAll(data)
            alist = s.cursor.fetchall()
            for vo in alist:
                print(vo)
        # 删除
        if int(opt) == 2:
            data = int(input('input the student\'s id which u want to delete:'))
            s.delete(data)
        # 添加
        if int(opt) == 3:
            data =[]
            tup1 = input('name:')
            data.append(tup1)
            tup2 = int(input('age:'))
            data.append(tup2)
            tup3 = input('sex:')
            data.append(tup3)
            tup4 = input('classid:')
            data.append(tup4)
            data = tuple(data)
            s.insert(data)

# 判断当前是否是主运行程序，并调用
if  __name__ == '__main__':
    main()